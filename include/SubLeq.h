#ifndef SubLeq_H
#define SubLeq_H

#include <iostream>

class SubLeq
{
	/*! 
     * Public section
	*/
	public:
		/*! Functions */
		SubLeq();
		~SubLeq();

		void JMP(); /*! Unconditional branch */
		void ADD(); /*! Add a in b */
		void MOV(); /*! Replace b content with a */
		void BEQ(); /*! Assemble a branch-if-zero */
		void NOT(); /*! Higher-order instruction - operator not */

	/*! 
     * Protected section
	*/
	protected:

	/*! 
     * Private section
	*/
	private:
		/*! Attributes */

};

#endif // SubLeq_H