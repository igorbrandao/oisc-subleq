#include "SubLeq.h"

/********************************************//**
* \class constructor
***********************************************/
SubLeq::SubLeq()
{
	/* Empty */
}

/********************************************//**
* \class destructor
***********************************************/
SubLeq::~SubLeq()
{
	/* Empty */
}

/********************************************//**
* \Unconditional branch
***********************************************/
void SubLeq::JMP()
{
	// JMP c == subleq Z, Z, c
}

/********************************************//**
* \Add a in b
***********************************************/
void SubLeq::ADD()
{
	/*
		ADD a, b == subleq a, Z
			== subleq Z, b
			== subleq Z, Z 
	*/
}

/********************************************//**
* \Replace b content with a
***********************************************/
void SubLeq::MOV()
{
	/*
		MOV a, b == subleq b, b
			== subleq a, Z
			== subleq Z, b
			== subleq Z, Z
	*/
}

/********************************************//**
* \Replace b content with a
***********************************************/
void beq()
{
	/*
		BEQ b, c == subleq b, Z, L1
			== subleq Z, Z, OUT
			L1 subleq Z, Z
			subleq Z, b, c
			OUT:...
    */
}

void not()
{
	/*
 		NOT a == subleq2 tmp          ; tmp = 0 (tmp = temporary register)    
			subleq2 tmp          
			subleq2 minus_one    ; acc = -1
			subleq2 a            ; a' = a + 1
			subleq2 Z            ; Z  = Z - 1
			subleq2 tmp          ; tmp = a + 1
			subleq2 a            ; a' = 0
			subleq2 tmp          ; load tmp into acc
			subleq2 a            ; a'  = -a -1 ( = ~a )
			subleq2 Z            ; set Z back to 0
	*/
}

int main()
{
	/* Variable definitions */
	int memory[];
	int program_counter, a, b, c;

	/*
		integer memory[], program_counter a, b, c, 
		program_counter = 0
		while (program counter >= 0):
		   a = memory [program_counter]
		   b = memory [program_counter + 1]
		   c = memory[program_counter + 2]
		   if ( a < 0 or b < 0):
		        program_counter = -1
		   else:
		       memory[b] = memory[b] - memory[a]
		       if (memory[b] > 0):
		           program_counter = program_counter + 3
		       else:
		           program_counter = c
	*/
}