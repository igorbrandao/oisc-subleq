/********************************************//**
* \Subleq.cpp
* \Main
***********************************************/
#include <iostream>
//#include "SubLeq.h"

using namespace std;

int memory[2048];
int pc = 0, a, b, c, addr, va, vb;


void c0()
{
	if ( pc >= 0 )
	{
		addr = pc;
		a = memory [addr];

		addr = pc + 1;
		b = memory [addr];

		addr = pc + 2;
		c = memory [addr];

		if ( a < 0 or b < 0 )
		{
			pc = -1;
		}
		else
		{
			addr = b;
			vb = memory[addr];

			addr = a;
			va = memory[addr];

			addr = b;
			vb = vb - va;

			memory[addr] = vb;
		}

		if (vb > 0)
		{
		   pc = pc + 3;
		}
		else
		{
		   pc = c;
		}
	}   
}


int main()
{
	pc = 3;
	memory[0] = 0;
	memory[1] = 2;
	memory[2] = 3;

	memory[3] = 1;
	memory[4] = 0;
	memory[5] = 0;
	memory[6] = 2;
	memory[7] = 0;
	memory[8] = 0;

	c0();
	c0();
	c0();

	cout << memory[1] << ' ' << pc << endl;

}
