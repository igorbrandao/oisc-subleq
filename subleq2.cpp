/********************************************//**
* \Subleq.cpp
* \Main
***********************************************/
#include <iostream>
//#include "SubLeq.h"

using namespace std;

int memory[2048];
int pc = 0, a, b, c, addr, va, vb;

int main()
{
	pc = 3;
	memory[0] = 0;
	memory[1] = 2;
	memory[2] = 3;

	memory[3] = 1;
	memory[4] = 0;
	memory[5] = 0;
	memory[6] = 2;
	memory[7] = 0;
	memory[8] = 0;

	while ( pc >= 0 )
	{
		a = memory[pc];
		b = memory[pc + 1];
		c = memory[pc + 2];

		if ( a < 0 || b < 0 )
			pc = -1;
		else
			memory[b] = memory[b] - memory[a];

		if ( memory[b] > 0)
			pc = pc + 3;
		else
			pc = c;
	}

	cout << memory[a] << ' ' << pc << endl;
	cout << memory[b] << ' ' << pc << endl;
	cout << memory[c] << ' ' << pc << endl;
}
